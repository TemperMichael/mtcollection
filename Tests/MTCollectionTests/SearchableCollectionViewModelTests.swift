import XCTest
@testable import MTCollection

class TestSearchableCollectionViewModel: SearchableCollectionViewModel {
    var title: String? = nil
    var sections: [CollectionItem] = []
    var searchText: String = ""
    
    required init(sections: [CollectionItem], title: String? = nil, searchText: String = "") {
        self.sections = sections
        self.title = title
        self.searchText = searchText
    }

    func setupSections(_ sections: [CollectionItem]) { }
}

final class SearchableCollectionViewModelTests: XCTestCase {
    
    var firstItems: [CollectionItem]!
    var sections: [CollectionItem]!
    var viewModel: SearchableCollectionViewModel!
    
    override func setUpWithError() throws {
        firstItems = [
            CollectionItem(title: "iTeM nR. 1", subtitle: "Item"),
            CollectionItem(title: "iTEM Nr. 2", subtitle: "Item"),
            CollectionItem(title: "This is the third item", subtitle: "Item"),
            CollectionItem(title: "ITEM NR. 4", subtitle: "Item"),
        ]
        
        sections = [
            CollectionItem(title: "fIrsT", subtitle: "Section", items: firstItems),
            CollectionItem(title: "sECOND", subtitle: "Section"),
            CollectionItem(title: "Third", subtitle: "Section"),
            CollectionItem(title: "This is the fourth section", subtitle: "Section"),
            CollectionItem(title: "FIFTH", subtitle: "Section"),
        ]
        
        viewModel = TestSearchableCollectionViewModel(sections: sections)
    }
    
    override func tearDownWithError() throws {
        firstItems = nil
        sections = nil
        viewModel = nil
        
        try super.tearDownWithError()
    }
    
    func testSections() {
        var results = viewModel.searchSections(viewModel.sections, for: "Not included")
        XCTAssertEqual(results, [], "The return value contains an unwanted value")
        
        results = viewModel.searchSections(viewModel.sections, for: "Section")
        XCTAssertEqual(results,
                       viewModel.sections,
                       "Amount of searched sections is incorrect")
        
        results = viewModel.searchSections(viewModel.sections, for: "First")
        XCTAssertEqual(results,
                       [viewModel.sections.first],
                       "Camelcase search does not work correctly")
        
        
        results = viewModel.searchSections(viewModel.sections, for: "FIRST")
        XCTAssertEqual(results,
                       [viewModel.sections.first],
                       "Uppercase search does not work correctly")
        
        results = viewModel.searchSections(viewModel.sections, for: "first")
        XCTAssertEqual(results,
                       [viewModel.sections.first],
                       "Lowercase search does not work correctly")
        
        results = viewModel.searchSections(viewModel.sections, for: "fiRSt")
        XCTAssertEqual(results,
                       [viewModel.sections.first],
                       "Mixed case search does not work correctly")
        
        results = viewModel.searchSections(viewModel.sections, for: "This is the fourth section")
        XCTAssertEqual(results,
                       [viewModel.sections[3]],
                       "Sentence search does not work correctly")
        
        results = viewModel.searchSections(viewModel.sections, for: "Thi")
        XCTAssertEqual(results,
                       [viewModel.sections[0], viewModel.sections[2], viewModel.sections[3]],
                       "Part of word search does not work correctly")
        
        results = viewModel.searchSections(viewModel.sections, for: "Item")
        XCTAssertEqual(results,
                       [viewModel.sections.first],
                       "Section with items that contain the searched term does not work correctly")
        
        results = viewModel.searchSections(viewModel.sections, for: "2")
        XCTAssertEqual(results,
                       [viewModel.sections.first],
                       "Number search does not work correctly")
        
        results = viewModel.searchSections(viewModel.sections, for: "")
        XCTAssertEqual(results,
                       viewModel.sections,
                       "Empty text search does not work correctly")
        
        results = viewModel.searchSections([], for: "First")
        XCTAssertTrue(results.isEmpty,
                      "Empty collection search does not work correctly")
    }
    
    func testItems() {
        guard let section = viewModel.sections.first else {
            XCTFail("First section not set")
            return
        }
        
        var results = viewModel.searchItems(for: "Not included", in: section)
        XCTAssertEqual(results,
                       [],
                       "The return value contains an unwanted value")
        
        results = viewModel.searchItems(for: "Item", in: section)
        XCTAssertEqual(results,
                       section.items,
                       "Amount of searched sections is incorrect")
        
        results = viewModel.searchItems(for: "Nr", in: section)
        XCTAssertEqual(results,
                       [section.items?[0], section.items?[1], section.items?[3]],
                       "Camelcase search does not work correctly")
        
        results = viewModel.searchItems(for: "NR", in: section)
        XCTAssertEqual(results,
                       [section.items?[0], section.items?[1], section.items?[3]],
                       "Uppercase search does not work correctly")
        
        results = viewModel.searchItems(for: "nr", in: section)
        XCTAssertEqual(results,
                       [section.items?[0], section.items?[1], section.items?[3]],
                       "Lowercase search does not work correctly")
        
        results = viewModel.searchItems(for: "nR", in: section)
        XCTAssertEqual(results,
                       [section.items?[0], section.items?[1], section.items?[3]],
                       "Mixed case search does not work correctly")
        
        results = viewModel.searchItems(for: "This is the third item", in: section)
        XCTAssertEqual(results,
                       [section.items?[2]],
                       "Sentence search does not work correctly")
        
        results = viewModel.searchItems(for: "Thi", in: section)
        XCTAssertEqual(results,
                       [section.items?[2]],
                       "Part of word search does not work correctly")
        
        results = viewModel.searchItems(for: "2", in: section)
        XCTAssertEqual(results,
                       [section.items?[1]],
                       "Number search does not work correctly")
        
        results = viewModel.searchItems(for: "", in: section)
        XCTAssertEqual(results,
                       section.items,
                       "Empty text search does not work correctly")
    }
}
