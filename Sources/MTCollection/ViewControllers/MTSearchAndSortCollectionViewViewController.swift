//
//  MTSearchableCollectionViewViewController.swift
//
//  Created by Michael Temper on 16.10.22.
//

import UIKit

open class MTSearchAndSortCollectionViewViewController: UIViewController {
    
    private let viewModel: SearchAndSortCollectionViewModel
    private var dataSource: UICollectionViewDiffableDataSource<Int, CollectionItem>?
    
    private let searchController: UISearchController = {
        $0.obscuresBackgroundDuringPresentation = false
        $0.definesPresentationContext = true
        return $0
    }(UISearchController())
    
    private lazy var collectionView: UICollectionView = {
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UICollectionView(frame: view.bounds, collectionViewLayout: createLayout()))
    
    public init(viewModel: SearchAndSortCollectionViewModel) {
        self.viewModel = viewModel

        super.init(nibName: nil, bundle: nil)
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.setupSections(viewModel.sections)
        configureSearchController()
        
        configureDataSource()
        configureSnapshot()
        
        setupView()
        setupNavigationBar()
    }
    
    private func createLayout() -> UICollectionViewLayout {
        return UICollectionViewCompositionalLayout { section, layoutEnvironment in
            var config = UICollectionLayoutListConfiguration(appearance: .insetGrouped)
            config.headerMode = .firstItemInSection
            return NSCollectionLayoutSection.list(using: config,
                                                  layoutEnvironment: layoutEnvironment)
        }
    }
    
    private func configureSearchController() {
        searchController.searchResultsUpdater = self
        navigationItem.searchController = searchController
    }
    
    private func configureDataSource() {
        let headerRegistration = UICollectionView.CellRegistration<UICollectionViewListCell, CollectionItem> { cell, indexPath, item in
            var content = cell.defaultContentConfiguration()
            content.text = item.title
            content.secondaryText = item.subtitle
            cell.contentConfiguration = content
            
            cell.accessories = [.outlineDisclosure()]
        }
        
        let cellRegistration = UICollectionView.CellRegistration<UICollectionViewListCell, CollectionItem> { cell, indexPath, item in
            var content = cell.defaultContentConfiguration()
            content.text = item.title
            content.secondaryText = item.subtitle
            cell.contentConfiguration = content
            
            cell.accessories = [.disclosureIndicator()]
        }
        
        dataSource = UICollectionViewDiffableDataSource<Int, CollectionItem>(collectionView: collectionView) { collectionView, indexPath, item in
            collectionView.dequeueConfiguredReusableCell(using: indexPath.item == 0 ? headerRegistration : cellRegistration,
                                                         for: indexPath,
                                                         item: item)
        }
        
        collectionView.dataSource = dataSource
    }
    
    public func configureSnapshot() {
        var snapshot = dataSource?.snapshot() ?? NSDiffableDataSourceSnapshot<Int, CollectionItem>()
        
        let searchText = searchController.searchBar.text ?? ""
        let filteredSections = viewModel.searchSections(viewModel.sections, for: searchText)

        if snapshot.sectionIdentifiers.isEmpty {
            snapshot.appendSections(Array(0...filteredSections.count))
            dataSource?.apply(snapshot, animatingDifferences: false)
        }
        
        filteredSections.enumerated().forEach { index, section in
            var sectionSnapshot = NSDiffableDataSourceSectionSnapshot<CollectionItem>()
            let headerItem = CollectionItem(title: section.title)
            sectionSnapshot.append([headerItem])
            
            let items = viewModel.searchItems(for: searchText, in: section).compactMap {
                CollectionItem(title: $0.title, subtitle: $0.subtitle)
            }
            
            sectionSnapshot.append(items, to: headerItem)
            sectionSnapshot.expand([headerItem])
            dataSource?.apply(sectionSnapshot, to: index, animatingDifferences: false)
        }
    }
    
    private func setupView() {
        view.addSubview(collectionView)
                
        NSLayoutConstraint.activate([
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.topAnchor.constraint(equalTo: view.topAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    open func setupNavigationBar() {
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.title = viewModel.title

        setupSortMenu()
    }

    private func setupSortMenu() {
        let forwardComparator = [KeyPathComparator(\CollectionItem.title, order: .forward)]
        let reverseCompatator = [KeyPathComparator(\CollectionItem.title, order: .reverse)]

        let sectionMenuElement = UIDeferredMenuElement.uncached { [weak self] completion in
            let actions = [
                UIAction(title: "A - Z", state: self?.viewModel.sectionSortOder == forwardComparator ? .on : .off) { [weak self] _ in
                    self?.viewModel.sectionSortOder = forwardComparator
                    self?.configureSnapshot()
                },
                UIAction(title: "Z - A", state: self?.viewModel.sectionSortOder == reverseCompatator ? .on : .off) { [weak self] _ in
                    self?.viewModel.sectionSortOder = reverseCompatator
                    self?.configureSnapshot()
                }
            ]
            completion(actions)
        }

        let itemMenuElement = UIDeferredMenuElement.uncached { [weak self] completion in
            let actions = [
                UIAction(title: "A - Z", state: self?.viewModel.itemSortOrder == forwardComparator ? .on : .off) { [weak self] _ in
                    self?.viewModel.itemSortOrder = forwardComparator
                    self?.configureSnapshot()
                },
                UIAction(title: "Z - A", state: self?.viewModel.itemSortOrder == reverseCompatator ? .on : .off) { [weak self] _ in
                    self?.viewModel.itemSortOrder = reverseCompatator
                    self?.configureSnapshot()
                }
            ]
            completion(actions)
        }

        let sectionMenu = UIMenu(title: viewModel.sectionMenuTitle, children: [sectionMenuElement])
        let itemMenu = UIMenu(title: viewModel.itemMenuTitle, children: [itemMenuElement])
        let menuBarButton = UIBarButtonItem(title: viewModel.sortingTitle, menu: UIMenu(children: [sectionMenu, itemMenu]))
        navigationItem.rightBarButtonItem = menuBarButton
    }
}

extension MTSearchAndSortCollectionViewViewController: UISearchResultsUpdating {
    
    public func updateSearchResults(for searchController: UISearchController) {
        configureSnapshot()
    }
}
