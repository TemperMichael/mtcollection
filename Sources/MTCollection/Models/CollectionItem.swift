//
//  CollectionItem.swift
//  MTExamples
//
//  Created by Michael Temper on 16.10.22.
//

import Foundation

public struct CollectionItem: Hashable, Identifiable {
    public let id: String
    public let title: String
    public let subtitle: String?
    public let imageName: String?

    public var isSelected: Bool
    public var isLoading: Bool
    public var items: [CollectionItem]?
    
    public init(id: String = UUID().uuidString,
                title: String,
                subtitle: String? = nil,
                imageName: String? = nil,
                isSelected: Bool = false,
                isLoading: Bool = false,
                items: [CollectionItem]? = nil) {
        self.id = id
        self.title = title
        self.subtitle = subtitle
        self.imageName = imageName
        self.isSelected = isSelected
        self.isLoading = isLoading
        self.items = items
    }

    public mutating func sortItems(using comparator: [KeyPathComparator<Self>]) {
        items?.sort(using: comparator)
    }
}

