//
//  CollectionViewModel.swift
//  MTExamples
//
//  Created by Michael Temper on 16.10.22.
//

import Foundation

public protocol CollectionViewModel: AnyObject {
    var title: String? { get }
    var sections: [CollectionItem] { get set }

//    init(sections: [CollectionItem], title: String?)
    
    func setupSections(_ sections: [CollectionItem])
}

public extension CollectionViewModel {

    var allItems: [CollectionItem] {
        sections.flatMap { $0.items ?? [] }
    }
}
