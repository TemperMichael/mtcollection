//
//  SearchableCollection.swift
//  MTExamples
//
//  Created by Michael Temper on 16.10.22.
//

import Foundation

public protocol SearchableCollection {
    var searchText: String { get set }
    
    func searchItems(for searchText: String, in section: CollectionItem) -> [CollectionItem]
    func searchSections(_ sections: [CollectionItem], for searchText: String) -> [CollectionItem]
}

public extension SearchableCollection {

    func searchSections(_ sections: [CollectionItem], for searchText: String) -> [CollectionItem] {
        if searchText.isEmpty {
            return sections
        } else {
            let text = searchText.lowercased()
            return sections.filter {
                $0.title.lowercased().contains(text) || $0.subtitle?.lowercased().contains(text) == true || !searchItems(for: text, in: $0).isEmpty
            }
        }
    }
    
    func searchItems(for searchText: String, in section: CollectionItem) -> [CollectionItem] {
        if searchText.isEmpty {
            return section.items ?? []
        } else {
            let text = searchText.lowercased()
            return section.items?.filter { $0.title.lowercased().contains(text) || $0.subtitle?.lowercased().contains(text) == true } ?? []
        }
    }

    func searchAllItems(for searchText: String, in sections: [CollectionItem]) -> [CollectionItem] {
        let items = sections.flatMap { $0.items ?? [] }
        if searchText.isEmpty {
            return items
        } else {
            let text = searchText.lowercased()
            return items.filter {
                $0.title.lowercased().contains(text) || $0.subtitle?.lowercased().contains(text) == true
            }
        }
    }
}

public extension SearchableCollection where Self: CollectionViewModel {

    var searchResults: [CollectionItem] { searchSections(sections, for: searchText) }

    func searchSections(for searchText: String) -> [CollectionItem] {
        searchSections(sections, for: searchText)
    }

    func searchAllItems(for searchText: String) -> [CollectionItem] {
        searchAllItems(for: searchText, in: sections)
    }
}
