//
//  SearchableCollectionViewModel.swift
//  MTExamples
//
//  Created by Michael Temper on 16.10.22.
//

import Foundation

public protocol SearchableCollectionViewModel: CollectionViewModel, SearchableCollection { }

public protocol SortableCollectionViewModel: CollectionViewModel, SortableCollection { }

public extension SortableCollectionViewModel {

    func setupSections(_ sections: [CollectionItem]) {
        self.sections = sortItems(for: sortSections(sections))
    }
}

public protocol SelectableCollectionViewModel: CollectionViewModel, SelectableCollection { }

public protocol SearchAndSortCollectionViewModel: SortableCollectionViewModel, SearchableCollection { }

public protocol SearchSortSelectCollectionViewModel: SortableCollectionViewModel, SearchableCollection, SelectableCollection { }

