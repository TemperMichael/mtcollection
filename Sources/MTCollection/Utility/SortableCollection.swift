//
//  SortableCollection.swift
//  
//
//  Created by Michael Temper on 09.11.22.
//

import Foundation

public protocol SortableCollection: AnyObject {

    var sectionMenuTitle: String { get }
    var itemMenuTitle: String { get }
    var sortingTitle: String { get }

    var sectionSortOder: [KeyPathComparator<CollectionItem>] { get set }
    var itemSortOrder: [KeyPathComparator<CollectionItem>] { get set }

    func sortSections(_ sections: [CollectionItem]) -> [CollectionItem]
    func sortItems(for sections: [CollectionItem]) -> [CollectionItem]
}

public extension SortableCollection {

    var sortingTitle: String {
        String(localized: "sorting.title", bundle: .module, comment: "The title of the sort navigationbar item.")
    }

    var sectionMenuTitle: String {
        String(localized: "sorting.sections.title", bundle: .module, comment: "The title of the sort section menu item.")
    }

    var itemMenuTitle: String {
        String(localized: "sorting.items.title", bundle: .module, comment: "The title of the sort item menu item.")
    }

    func sortSections(_ sections: [CollectionItem]) -> [CollectionItem] {
        return sections.sorted(using: sectionSortOder)
    }

    func sortItems(for sections: [CollectionItem]) -> [CollectionItem] {
        var mutableSections = sections
        mutableSections.enumerated().forEach { index, _ in
            mutableSections[index].sortItems(using: itemSortOrder)
        }
        return mutableSections
    }
}

public extension SortableCollection where Self: CollectionViewModel {

    func sortSections() {
        sections = sortSections(sections)
    }

    func sortAllItems() {
        sections = sortItems(for: sections)
    }
}

private enum SelectionType {
    case select
    case deselect
    case toggle
}

public protocol SelectableCollection: AnyObject {

    func select(id: String, in sections: inout [CollectionItem])
    func deselect(id: String, in sections: inout [CollectionItem])
    func toggle(id: String, in sections: inout [CollectionItem])

    func setAllItems(to selected: Bool, in sections: inout [CollectionItem])
    func selectAllItems(in sections: inout [CollectionItem])
    func deselectAllItems(in sections: inout [CollectionItem])
    func toggleAllItems(in sections: inout [CollectionItem])
}

public extension SelectableCollection {

    func select(id: String, in sections: inout [CollectionItem]) {
        set(type: .select, for: id, in: &sections)
    }

    func deselect(id: String, in sections: inout [CollectionItem]) {
        set(type: .deselect, for: id, in: &sections)
    }

    func toggle(id: String, in sections: inout [CollectionItem]) {
        set(type: .toggle, for: id, in: &sections)
    }

    func setAllItems(to selected: Bool, in sections: inout [CollectionItem]) {
        setAllItems(type: selected ? .select : .deselect, in: &sections)
    }

    func selectAllItems(in sections: inout [CollectionItem]) {
        setAllItems(type: .select, in: &sections)
    }

    func deselectAllItems(in sections: inout [CollectionItem]) {
        setAllItems(type: .deselect, in: &sections)
    }

    func toggleAllItems(in sections: inout [CollectionItem]) {
        setAllItems(type: .toggle, in: &sections)
    }

    private func set(type: SelectionType, for id: String, in sections: inout [CollectionItem]) {
        if let sectionIndex = sections.firstIndex(where: { $0.id == id }) {
            switch type {
            case .select:
                sections[sectionIndex].isSelected = true
            case .deselect:
                sections[sectionIndex].isSelected = false
            case .toggle:
                sections[sectionIndex].isSelected.toggle()
            }
        } else if let sectionIndex = sections.firstIndex(where: { section in section.items?.contains(where: { $0.id == id }) == true }),
                  let itemIndex = sections[sectionIndex].items?.firstIndex(where: { $0.id == id }) {
            switch type {
            case .select:
                sections[sectionIndex].items?[itemIndex].isSelected = true
            case .deselect:
                sections[sectionIndex].items?[itemIndex].isSelected = false
            case .toggle:
                sections[sectionIndex].items?[itemIndex].isSelected.toggle()
            }
        }
    }

    private func setAllItems(type: SelectionType, in sections: inout [CollectionItem]) {
        sections.enumerated().lazy.forEach { sectionIndex, section in
            section.items?.enumerated().lazy.forEach { itemIndex, _ in
                switch type {
                case .select:
                    sections[sectionIndex].items?[itemIndex].isSelected = true
                case .deselect:
                    sections[sectionIndex].items?[itemIndex].isSelected = false
                case .toggle:
                    sections[sectionIndex].items?[itemIndex].isSelected.toggle()
                }
            }
        }
    }
}

public extension SelectableCollection where Self: CollectionViewModel {
    
    func select(id: String) {
        select(id: id, in: &sections)
    }

    func deselect(id: String) {
        deselect(id: id, in: &sections)
    }

    func toggle(id: String) {
        toggle(id: id, in: &sections)
    }

    func setAllItems(to selected: Bool) {
        setAllItems(to: selected, in: &sections)
    }
}
